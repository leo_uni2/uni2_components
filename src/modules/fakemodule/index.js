import React, { useEffect, useState, useRef } from "react";
import { useForm } from "react-hook-form";

const FakeModule = () => {
  const ref = useRef(null);
  const {
    handleSubmit,
    formState: { errors, values },
    setError,
    register,
    watch,
  } = useForm();

  const watchAllFields = watch(); // when pass nothing as argument, you are watching everything

  useEffect(() => {
    console.log("WATCHING");
    const subscription = watch((formData) => {
      console.log({ formData });
      if (ref.current) {
        ref.current.setAttribute("data-values", JSON.stringify(formData));
      }
    });
    return () => {
      subscription.unsubscribe();
    };
  }, [watch]);

  useEffect(() => {
    window.addEventListener(
      "message",
      ({ data: { u2Msg } }) => {
        if (u2Msg && u2Msg.type === "initialData") {
          setProducts(Object.values(u2Msg.payload) || []);
        }

        if (u2Msg && u2Msg.type === "formFeedback") {
          // setError()
          console.log("formFeedback: ", u2Msg.payload);
          Object.keys(u2Msg.payload).forEach((key) => {
            setError(key, {
              type: "manual",
              message: u2Msg.payload[key][0],
            });
          });
          // setProducts(Object.values(u2Msg.payload) || []);
        }

        if (u2Msg && u2Msg.type === "requestSubmit") {
          window.parent.postMessage(
            {
              u2Msg: {
                type: "formData",
                payload: JSON.parse(ref.current.getAttribute("data-values")),
              },
            },
            "*"
          );
        }
      },
      false
    );

    window.parent.postMessage({ u2Msg: { type: "ready" } }, "*");

    return () => {
      window.removeEventListener("message", null);
    };
  }, []);

  const [products, setProducts] = useState([]);

  return (
    <>
      <form
        ref={ref}
        onSubmit={handleSubmit((values) => {
          console.log({ values });
        })}
        className="grid grid-cols-3"
      >
        <div>
          <input
            {...register("tipo_idenficacion")}
            className="p-3 border border-gray-600"
          />
          ~
          {errors.tipo_identificacion && (
            <span className="text-red-500 font-xs">
              <br />
              {errors.tipo_identificacion.message}
            </span>
          )}
        </div>
        <div>
          <input
            {...register("numero_identificacion")}
            className="p-3 border border-gray-600"
          />
          {errors.numero_identificacion && (
            <span className="text-red-500 font-xs">
              <br />
              {errors.numero_identificacion.message}
            </span>
          )}
        </div>
        <select {...register("producto")}>
          {products.map((product) => (
            <option key={product.value} value={product.value}>
              {product.label}
            </option>
          ))}
        </select>
      </form>
    </>
  );
};

export default FakeModule;
