import React from "react";
import { useForm } from "react-hook-form";

export function Form({ defaultValues, children, onSubmit, ...props }) {
  const { handleSubmit, register } = useForm({ defaultValues });

  return (
    <form onSubmit={handleSubmit(onSubmit)} {...props}>
      {Array.isArray(children)
        ? children.map((child) => {
            return child.props.name
              ? React.createElement(child.type, {
                  ...{
                    ...child.props,
                    register,
                    key: child.props.name,
                  },
                })
              : child;
          })
        : children}
    </form>
  );
}

export function Input({ register, label, name, ...rest }) {
  return (
    <div>
      {label && (
        <>
          <label htmlFor={name}>{label}</label>
          <br />
        </>
      )}
      <input {...register(name)} {...rest} className="border border-gray-500" />
    </div>
  );
}

export function Select({ register, options, name, label, ...rest }) {
  return (
    <div>
      {label && (
        <>
          <label htmlFor={name}>{label}</label>
          <br />
        </>
      )}
      <select {...register(name)} {...rest} className="border border-gray-500">
        {options.map((item, index) => (
          <option key={index} value={item.value}>
            {item.label}
          </option>
        ))}
      </select>
    </div>
  );
}
