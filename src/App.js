import FakeModule from "./modules/fakemodule";

function App() {
  return (
    <div className="App">
      <FakeModule />
    </div>
  );
}

export default App;
